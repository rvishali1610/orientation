**GIT BASICS**

_**Git**_ - - 
-            Distributed version-control system
-            Way we manage code
- 			 In software development

![Click here](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/11/Git-Architechture-Git-Tutorial-Edureka-2-768x720.png)
			
**VOCABULARY**

1. _**Repository**_ 
                    - Collection of files and folders

-                     Tracks all changes in git



 
1. _**Commit**_ - Save changes to local repo
 
1. _**Push**_ - Transfer commits from local repo to remote repo
 
1. _**Branch**_- ![Click here](https://www.nobledesktop.com/image/gitresources/git-branches-merge.png)
 
1. _**Merge**_ - ![Click here](https://res.cloudinary.com/practicaldev/image/fetch/s--MEKaM3dY--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://cl.ly/430Q2w473e2R/Image%25202018-04-30%2520at%25201.07.58%2520PM.png)

1. _**Clone**_ - Exact copy of entire repo on your local machine

1. _**Fork**_ - Duplicate of existing repo under your own name


**GIT INTERNALS**
![Click here](https://lh3.googleusercontent.com/7W73y7YIzc9H3YK2tjx9gNDvgMs6hJnCGzh8dcHPYC2rWqAyX93zMUaRYtTs46udvhtCx-jo6d8WMKdy7ZGtYHN_bO6k7Kd_wZm_rY0dBGdR-NqgAgn7YiRP3po=s412)

**GIT WORKFLOW**
![Click here](https://s3-us-west-2.amazonaws.com/dgw-blog/2018/05/GIt-Workflow-Diagram--5-.png)

**BASIC GIT COMMANDS**

1. **git init** - To initialize empty git repo

1. **git add <file or directory name>** - To add files to staging area for Git

1. **git commit -m "message"** - To record changes made to files in local repo

1. **git status** - To return current state of repo

1. **git branch <branch name>** - To determine what branch you are in

1. **git branch -a <branch name>** - To add branch

1. **git branch -d <branch name>** - To delete branch

1. **git checkout** - To switch branches

1. **git merge <branch_name>** - To combine the change from one branch to another branch
 
1. **git clone <remote-url>** - To copy and download the repo to computer

1. **git pull <branch-name> <remote-url>** - To get the latest version of repo

1. **git push <remote-url> <branch-name>** - To send local commits to remote repo
