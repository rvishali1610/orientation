**DOCKER BASICS**

**WHY DOCKER?** - To build large projects and to deploy them in various environments

**VOCABULARY**

1. **DOCKER** - program for developers to develop and run applications with containers
 ![Click here](https://thingsolver.com/wp-content/uploads/pasted-image-0-768x452-1.png)
1. **DOCKER IMAGE** - Contains code,runtime,libraries,environment variables and configuration files which is needed to run applications as container
 ![Click here](https://miro.medium.com/max/700/1*p8k1b2DZTQEW_yf0hYniXw.png)
1. **CONTAINER**	- Running docker image 
1. **DOCKER HUB** - Github for docker images and containers
![Click here](https://sumitc91.github.io/Blogs/8f5fb6c4-3070-4ce0-a3a9-ea0070a3fc7c_docker.jpg)

**BASIC DOCKER COMMANDS**

1. **$ docker ps** - List all containers running on docker host
1. **$ docker start <docker name/ID>** - Starts any stopped container
1. **$ docker stop <docker name/ID>** - Stops any running container
1. **$ docker run <docker name/ID>** - Creates container from docker images
1. **$ docker rm <docker name/ID>** - Deletes the container
1. **$ docker pull <docker name/ID>** - Download docker
1. **$ docker cp <file name>** - Copy the code inside docker


**DOCKER WORKFLOW**

![Click here](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/docker-app-development-workflow/life-cycle-containerized-apps-docker-cli.png)